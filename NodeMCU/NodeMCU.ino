#include "headers.h"

#define BLINK_TIME 1000 // duration in ms

unsigned long time_ref = 0; 
int message_once = 0;
 
void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  delay(100);
  
  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);    // turn the LED off by default
  pinMode(14, INPUT_PULLUP);

  // set Wifi
  wifi_setup();

  // Initialize TCP client socket
  socket_setup();
}
 

// the loop function runs over and over again forever
void loop() {
  // put your main code here, to run repeatedly:

  
  // Try to reco when connection is lost
  socket_reco();  

  //blinking LED
  if((millis() - time_ref) >= BLINK_TIME)
  {
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW  
    time_ref = millis();
  }
  else if((millis() - time_ref) >= (BLINK_TIME / 2))
  {
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  }


  // main function to manage the mailbox
  mailbox_FSM();  

}
  
