#include "headers.h"

void mailbox_FSM()
{
  enum states{
    Reset,
    Wait,
    Mail
  };

  static states state;
  static states next_state;
  static int input;

  static bool old_GPIO; // reminder: high is not an input !!

  if (digitalRead(14) != old_GPIO)
  {
    //debounce
    delay(1500);
    
    old_GPIO = digitalRead(14);
    input = 1;  
  }
  else
  {
    input = 0;  
  }

  state = next_state;


  // Main switch for the FSM
  switch (state)
  {
    case Reset: // Frist state, when the board is reset
      next_state = Wait;
    break;

    case Wait: // State when there is nothing in the mailbox
      if (input == 1) // something came to the mailbox !
      {
        next_state = Mail;
        socket_send("Mail");
      }
      else  // Still nothing 
      {
        next_state = Wait;
      }
    break;

    case Mail:// State when there is unclaimed mails in the mailbox
      if (input == 0) // still something in the mailbox
      {
        next_state = Mail;
      }
      else  // The mail was taken !
      {
        next_state = Wait;
        socket_send("NoMail");
      }
    break;

    default:
      next_state = Reset;
  }

  
}

