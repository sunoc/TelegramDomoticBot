
#include "headers.h"
#include "wifi_conf.h" // must contain the defines for the SSID and the PASSWORD


 
// SSID and PASSWORD are defined in the wifi_conf.h file
const char* ssid = SSID;
const char* password = PASSWORD;


void wifi_setup(){

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
 // connection "progress bar"
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());
}

