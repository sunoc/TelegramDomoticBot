#include "headers.h"
#include "socket_conf.h" //configuration values for the TCP socket

// Use WiFiClient class to create TCP connections
WiFiClient client;

const uint16_t port = 5005;
const char * host = SERVER_IP; // ip


void wait_co(int milisec){
  Serial.println("connection failed");
  Serial.println("wait...");
  delay(milisec);
}


void socket_co(int error_wait){
  if(!client.connect(host, port)){
    wait_co(error_wait);          
  }
  else{
    Serial.print("successfully connected to");
    Serial.println(host);
    return;
  }
}


void socket_reco(){
  if (!client.connected()) {
    socket_co(60000); // try to connect, wait 1 min if fails
  }
}


void socket_setup(){

  // loop until connected
  while(1){

    Serial.print("Create a TCP connection\n");
    Serial.print("connecting to ");
    Serial.println(host);

    socket_co(5000); // try to connect, wait 5 sec if fails

    if (client.connected()) { return; }
  }
  
}


void socket_send(char* message){
  
  // This will send the message to the server
  client.println(message);

  return;
  
}
