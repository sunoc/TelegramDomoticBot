#ifndef HEADERS_H_   /* Include guard */
#define HEADERS_H_

#include <ESP8266WiFi.h>

void wifi_setup();  
void socket_setup();
void socket_send(char* message);
void socket_co(int error_wait);
void socket_reco();
void wait_co(int milisec);
void socket_close();
void mailbox_FSM();

#endif // HEADERS_H_
