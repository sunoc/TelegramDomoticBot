#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import telegram
import logging


from telegram.ext import CommandHandler, Updater, MessageHandler, Filters
# from emoji import emojize


###################################################
# Telegram bot setup

# /!\ a file named "token" must be created and only contain the telegram bot token
with open('token', 'r') as myfile:
  token = myfile.read().replace('\n', '')

print "Telegram Token:"
print token
bot = telegram.Bot(token=token)
print(bot.get_me())

# shorten some variables names
updater = Updater(token=token)
dispatcher = updater.dispatcher
job = updater.job_queue


# -----------------
# Behavior when /start is sent to the bot
def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Salut, je suis le bot de domotique pour la famille Conus!")
dispatcher.add_handler(CommandHandler('start', start))


# -----------------
# Message to send when the GPIO is triggered
# /!\ a file named "chat_id" must be created and only contain the telegram chat ID
with open('chat_id', 'r') as myfile:
  chat_id = myfile.read().replace('\n', '')
def Mail(bot, job):
  bot.send_message(chat_id=chat_id, text="Il y a du courrier dans la boîte aux lettres!")
def NoMail(bot, job):
  bot.send_message(chat_id=chat_id, text="Le courrier a été récupéré!")
  
# -----------------  
# definition for unknown command
def unknown(bot, update):
  bot.send_message(chat_id=update.message.chat_id, text="Désolé, je ne comprend pas cette commande.")
dispatcher.add_handler(MessageHandler(Filters.command, unknown))

# Starts the bot
updater.start_polling()


###################################################
# TCP Socket connection

def connect():
  TCP_IP = '192.168.1.106' # this servers IP
  TCP_PORT = 5005 # communication port

  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  
  s.bind((TCP_IP, TCP_PORT))
  s.listen(1)

  global conn
  conn, addr = s.accept()

  print 'Connection address:', addr
      
  socketLoop()

def socketLoop():
  BUFFER_SIZE = 100  # Normally 1024, but we want fast response
  
  try:
    while 1:

      data = conn.recv(BUFFER_SIZE)

      if not data: break
      if data == "Mail":
        print "Mail"
        job.run_once(Mail, 1)
      elif data == "NoMail":
        print "NoMail"
        job.run_once(NoMail, 1)
  except socket.error:
    conn.close()
    connect()


connect()



